inputs = open('Day16/input').read()

# map size
width = len(inputs.split('\n')[0])
height = len(inputs.split('\n'))

# map the directions
directions = {
    'v' : (0,1),
    '>' : (1,0),
    '^' : (0,-1),
    '<' : (-1,0)
}

invert_directions = {
    (0,1) : 'v',
    (1,0) : '>',
    (0,-1) : '^',
    (-1,0) : '<'
}

elements = {
    '.' : 'empty',
    '|' : 'vertical_spliter',
    '-' : 'horizontal_spliter',
    '\\' : 'mirror 1',
    '/' : 'mirror 2',
}

def mirror1(direction):
    return (direction[1], direction[0])
    
def mirror2(direction):
    return (-direction[1], -direction[0])
    
def getDirection(char):
    return directions[char]

def getElement(char):
    return elements[char]

def move(x,y,direction):
    return (x + direction[0], y + direction[1])

def run(start_position, start_dir):
    x,y = start_position
    direction = getDirection(start_dir)
    while True:
        
        if (x < 0 or x >= width or y < 0 or y >= height):
            print("Out of bounds")
            return
        
        # check if the position is known
        if (known_trajectories.get((x,y))):
            # check if we already went through this position with the same direction
            if (invert_directions[direction] in known_trajectories[(x,y)]):
                # we are stuck in a loop
                return 

            else:
                # add the position to the known positions
                known_trajectories[(x,y)].append(invert_directions[direction])
                
        else:
            # add the position to the known positions
            known_trajectories[(x,y)] = [invert_directions[direction]]
        
        """ 
        path[y][x] = invert_directions[direction]
        # print the path as string
        for row in path:
            print(''.join(row))
        
        print(x,y, direction, map[y][x])
        """
        
        if (map[y][x] == '|'):
            # check if we go through the spliter or if we split
            if (direction[0] == 0):
                # same as if we had an empty space
                x,y = move(x,y,direction)
            else:
                # split into two directions
                run((x,y), '^')
                run((x,y), 'v')
                return
            continue

        if (map[y][x] == '-'):
            # check if we go through the spliter or if we split
            if (direction[1] == 0):
                # same as if we had an empty space
                x,y = move(x,y,direction)
            else:   
                # split into two directions
                run((x,y), '>')
                run((x,y), '<')
                return
            continue

        if (map[y][x] == '\\'):
            direction = mirror1(direction)
            x,y = move(x,y,direction)
            continue
        if (map[y][x] == '/'):
            direction = mirror2(direction)
            x,y = move(x,y,direction)
            continue
        if (map[y][x] == '.'):
            x,y = move(x,y,direction)
            continue
        print("Unknown element", map[y][x])
        return (x,y)



# main
map = inputs.split('\n')

path = [['.' for i in range(width)] for j in range(height)]

max = 0

# start the run from every edge
for i in range(width):
    known_trajectories = {}
    run((i,0), 'v')
    if (len(known_trajectories) > max):
        max = len(known_trajectories)
        
    
    known_trajectories = {}
    run((i,height-1), '^')
    if (len(known_trajectories) > max):
        max = len(known_trajectories)

for i in range(height):
    known_trajectories = {}
    run((0,i), '>')
    if (len(known_trajectories) > max):
        max = len(known_trajectories)
        
    known_trajectories = {}
    run((width-1,i), '<')
    if (len(known_trajectories) > max):
        max = len(known_trajectories)

print(max)
    
    
    
""" 
# known elements 
# dict of pos (int,int) -> dir (char)
known_trajectories = {}

# start the run at the top left corner
run((0,0), '>')

# Count the number of positions we went through
print(known_trajectories)
print(len(known_trajectories))
"""